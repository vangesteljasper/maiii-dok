const
  path = require(`path`),
  webpack = require(`webpack`),
  DashboardPlugin = require(`webpack-dashboard/plugin`),
  chalk = require(`chalk`),
  port = 3000,
  publicPath = process.env.DEST_PATH || `/`;

console.log(chalk.yellow(`ROOT: \'${publicPath}\'`));

const config = {
  entry: {
    main: [
      `./src/js/main/script.js`,
      `./src/css/main/styles.css`
    ],
    'home.critical': [
      `./src/css/critical/home.critical.css`
    ],
    'eventlist.critical': [
      `./src/css/critical/eventlist.critical.css`
    ],
    'eventdetail.critical': [
      `./src/css/critical/eventdetail.critical.css`
    ],
    loadDefferedStyles: [
      `./src/js/loadDefferedStyles.js`
    ]
  },
  output: {
    path: path.resolve(`./dist`),
    filename: `js/[name].js`,
    publicPath: publicPath
  },
  devServer: {
    contentBase: `./src`,
    historyApiFallback: true,
    hot: true,
    port: port
  },
  resolve: {
    extensions: [`.js`, `.css`]
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: [
          {loader: `babel-loader`},
          {loader: `eslint-loader`, options: {fix: true}}
        ]
      },
      {
        test: /\.(svg|png|jpe?g|gif|webp)$/,
        loader: `url-loader`,
        options: {
          limit: 1000, // inline if < 1 kb
          context: `src`,
          name: `[path][name].[ext]`
        }
      }
    ]
  },
  plugins: []
};

if (process.env.NODE_ENV === `production`) {
  const {UglifyJsPlugin} = webpack.optimize;
  const extractTextPlugin = require(`extract-text-webpack-plugin`);
  for (const entry in config.entry) {
    const regExpTests = [
      `(${entry}\.css)`, // search for css files with same name as entry
      `(${entry}\/.*\.css)` // search for all css files in folder named same as entry
    ];
    regExpTests.forEach(regString => {
      const loader = new extractTextPlugin(`css/${entry}.css`);
      config.module.rules.push({
        test: new RegExp(regString),
        loader: loader.extract([`css-loader?importLoaders=1`, `postcss-loader`])
      });
      config.plugins.push(loader);
    });
  }
  config.module.rules.push({
    test: /\.(svg|png|jpe?g|gif)$/,
    loader: `image-webpack-loader`,
    enforce: `pre`
  });
  config.plugins.push(new UglifyJsPlugin({sourceMap: true, comments: false}));
} else {
  config.devtool = `sourcemap`;
  config.entry.main.push(
    `./src/css/critical/home.critical.css`,
    `./src/css/critical/eventdetail.critical.css`,
    `./src/css/critical/eventlist.critical.css`
  );
  config.performance = {hints: false};
  config.module.rules.push({
    test: /\.css$/,
    use: [
      {loader: `style-loader`},
      {loader: `css-loader`, options: {importLoaders: 1}},
      {loader: `postcss-loader`}
    ]
  });
  config.plugins = [
    new DashboardPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ];
}

module.exports = config;
