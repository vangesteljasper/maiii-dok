<?php
require_once __DIR__ . '/DAO.php';
class ZoneDAO extends DAO {

  public function selectZones($offset = 0, $limit = 10) {
    $sql = "SELECT * FROM `ma3_dok_zones` ORDER BY `id` ASC LIMIT :offset, :limit";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':offset', $offset);
    $stmt->bindValue(':limit', $limit);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

}
