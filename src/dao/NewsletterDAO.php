<?php
require_once __DIR__ . '/DAO.php';
class NewsletterDAO extends DAO {

  public function addSubscriber($name, $email) {
    $sql = "INSERT INTO `ma3_dok_newsletter-subscribers` (`id`, `name`, `email`) VALUES (NULL, :name, :email)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':name', $name);
    $stmt->bindValue(':email', $email);
    if ($stmt->execute()) {
      return true;
    } else {
      return false;
    }
  }

}
