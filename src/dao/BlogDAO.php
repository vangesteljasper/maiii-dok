<?php
require_once __DIR__ . '/DAO.php';
class BlogDAO extends DAO {

  public function selectPosts($offset = 0, $limit = 10) {
    $sql = "SELECT * FROM `ma3_dok_blogposts` ORDER BY `published_date` DESC LIMIT :offset, :limit";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':offset', $offset);
    $stmt->bindValue(':limit', $limit);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
