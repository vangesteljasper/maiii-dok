import _ from 'underscore';
import createEvents from '../lib/createEvents';
import es6Promise from 'es6-promise';
import fetch from 'isomorphic-fetch';
es6Promise.polyfill();

const ROOT = `/`;

const $html = document.documentElement || document.getElementsByClassName(`html`)[0];
const $body = document.body || document.getElementsByClassName(`body`)[0];
const $main = document.getElementsByClassName(`content`)[0];
let $eventsList, $frag, offset = 10, count = 0, showing = 10;
let $loadMoreButton, $newsletterForm, $emailInput, $nameInput, $toTop;


const parseResults = result => {
  showing += result.data.length;

  // disable button if the amount of events on page is equal to
  // the total amount of events in the database
  console.log(`showing: ${showing}, count: ${count}`);
  if (showing >= count) {
    $loadMoreButton.setAttribute(`disabled`, ``);
    $loadMoreButton.classList.add(`disabled`);
  }

  // create events and append to documentfragment ($frag)
  $frag = document.createDocumentFragment();
  createEvents($frag, result.data);
  // append fragment to page
  $eventsList.appendChild($frag);
};

// fetch events from server
const fetchEvents = (offset, limit) => {
  fetch(`${ROOT}index.php?page=get&type=events&offset=${offset}&limit=${limit}&t=${Date.now()}`, {
    headers: new Headers({
      Accept: `application/json`
    }),
    method: `get`
  })
  .then(response => response.json())
  .then(result => {
    if (result.result === `ok`) {
      parseResults(result);
    } else {
      return false;
    }
  });
};

const pagination = () => {
  fetchEvents(offset, 10);
  offset += 10;
};

// remove message from php
const closeMessage = event => {
  let $message;
  if (event.currentTarget) $message = event.currentTarget.parentNode;
  else $message = event.parentNode;

  const $messages = document.getElementsByClassName(`messages`)[0];
  const count = $messages.childNodes.length;
  if (count > 1) {
    $messages.removeChild($message);
  } else {
    $body.removeChild($messages);
  }
};


// prevent link click
const prevent = event => {
  event.preventDefault();
  return false;
};

const goToTop = () => {
  const top = $html.scrollTop;
  const newTop = top - (top / 5);
  $html.scrollTop = newTop;
  if (newTop > 5) {
    requestAnimationFrame(goToTop);
  } else {
    $html.scrollTop = 0;
  }
};

const addEventListeners = () => {

  // broken links click
  const $brokenLinks = document.getElementsByClassName(`link_disabled`);
  _.each($brokenLinks, $link => {
    $link.addEventListener(`click`, prevent);
  });

  // server messages click
  const $messages = document.getElementsByClassName(`message__close`);
  _.each($messages, $message => {
    $message.addEventListener(`click`, closeMessage);
    setTimeout(() => {
      if ($body.contains($message)) {
        closeMessage($message);
      }
    }, 5000);
  });

  $emailInput.addEventListener(`input`, validateInput);
  $emailInput.addEventListener(`blur`, validateInput);
  $nameInput.addEventListener(`input`, validateInput);
  $nameInput.addEventListener(`blur`, validateInput);
  $newsletterForm.addEventListener(`submit`, formSubmit);

  $toTop.addEventListener(`click`, event => {
    event.preventDefault();
    requestAnimationFrame(goToTop);
  });
};

const formSubmit = event => {

  event.preventDefault();

  validateInput($nameInput);
  validateInput($emailInput);

  if ($newsletterForm.checkValidity()) {
    $newsletterForm.submit();
  }
};

const validateInput = input => {
  let $input;
  if (input.currentTarget) $input = input.currentTarget;
  else $input = input;

  const $error = $input.parentNode.getElementsByClassName(`input-error`)[0];

  if ($input.validity.valueMissing) {
    $error.textContent = `VERPLICHT`;
  } else if ($input.validity.typeMismatch) {
    $error.textContent = `INCORRECT`;
  } else {
    $error.textContent = ``;
  }
};

const init = () => {

  // if on events list page
  if ($main.classList.contains(`eventlist`)) {
    $loadMoreButton = document.getElementsByClassName(`ajax-load-more`)[0];
    if ($loadMoreButton) {

      $loadMoreButton.classList.remove(`hidden`);
      $loadMoreButton.classList.add(`link_disabled`);

      // event loading
      $eventsList = document.getElementsByClassName(`event-page__list`)[0];
      count = parseInt($eventsList.dataset.eventcount, 10);
      $loadMoreButton.addEventListener(`click`, pagination);
    }
  }

  $newsletterForm = document.getElementById(`newsletter`);
  $emailInput = document.getElementById(`email`);
  $nameInput = document.getElementById(`name`);
  $newsletterForm.noValidate = true;

  $toTop = document.getElementsByClassName(`to-top`)[0];

  addEventListeners();
};

init();
