export default (elmt, attr) => {
  for (const prop in attr) {
    elmt.setAttribute(prop, attr[prop]);
  }
};
