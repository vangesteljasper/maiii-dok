import set from './setAttributes';
import el from './createElement';

export default (frag, data) => {
  data.forEach(data => {
    let picture, source, image;

    const anchor = el(`a`);
    set(anchor, {
      href: data.link
    });

    const article = el(`article`);
    set(article, {
      class: `event-article`
    });

    if (data.image) {

      picture = el(`picture`);
      set(picture, {
        class: `event-article__image`
      });

      source = el(`source`);
      set(source, {
        type: `image/webp`,
        srcset: `${data.root}assets/img/events/${data.image}.webp`
      });

      image = el(`img`);
      set(image, {
        src: `${data.root}assets/img/events/${data.image}.jpg`,
        class: `event-article__image`,
        alt: data.title
      });

      picture.appendChild(source);
      picture.appendChild(image);

    } else {

      picture = el(`div`);
      set(picture, {
        class: `event-article__placeholder`
      });

    }

    const content = el(`div`);
    set(content, {
      class: `event-article__content`
    });

    const title = el(`h3`);
    set(title, {
      class: `title event-article__title`
    });
    title.innerText = data.title;

    const tags = el(`div`);
    set(tags, {
      class: `tags`
    });

    data.locations.forEach(location => {
      const t = el(`span`);
      set(t, {
        class: `tag tag_color_${location.color}`
      });
      t.innerText = location.name;
      tags.appendChild(t);
    });

    const date = el(`span`);
    set(date, {
      class: `tag`
    });
    date.innerText = `${data.start} / ${data.end}`;
    tags.appendChild(date);

    const paragraph = el(`p`);
    set(paragraph, {
      class: `event-article__paragraph`
    });
    paragraph.innerText = data.description;

    content.appendChild(title);
    content.appendChild(tags);
    content.appendChild(paragraph);

    article.appendChild(picture);
    article.appendChild(content);

    anchor.appendChild(article);

    frag.appendChild(anchor);
  });
};
