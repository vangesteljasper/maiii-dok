  <section class="above-footer">
    <div class="above-footer__newsletter">
      <div class="above-footer__newsletter__left">
        <h3 class="title title_color_white">nieuwsbrief.</h3>
        <p class="paragraph">Schrijf je in op onze nieuwsbrief om op de hoogte te blijven
          van nieuwe evenementen en belangrijke info.</p>
      </div>
      <div class="above-footer__newsletter__right">
        <form action="<?php echo ROOT ?>index.php?page=push&type=newsletter&src=<?php echo SRC ?>" method="post" id="newsletter" novalidate>
          <div>
            <div class="above-footer__newsletter__labels">
              <label class="label" for="name">naam</label>
              <span class="input-error"></span>
            </div>
            <input class="input input_color_opaque input_border_thick" type="text" id="name" name="name" required>
          </div>
          <div>
            <div class="above-footer__newsletter__labels">
              <label class="label" for="email">email</label>
              <span class="input-error"></span>
            </div>
            <input class="input input_color_opaque input_border_thick" type="email" id="email" name="email" required>
          </div>
          <div>
            <input class="submit submit_color_white submit_border_thick" type="submit" name="subscribe" value="meld aan">
          </div>
        </form>
      </div>
    </div>
    <div class="above-footer__social">
      <h3 class="title title_color_white">volg ons</h3>
      <div class="above-footer__social__icons">
        <a href="https://www.facebook.com/DOKgent" target="_blank" class="icon icon_size_medium icon_type_facebook"><span class="hidden">facebook</span></a>
        <a href="https://twitter.com/DOKGENT" target="_blank" class="icon icon_size_medium icon_type_twitter"><span class="hidden">twitter</span></a>
        <a href="https://www.instagram.com/dokgent/" target="_blank" class="icon icon_size_medium icon_type_instagram"><span class="hidden">instargram</span></a>
      </div>
    </div>
  </section>
</main>
<hr class="footer-seperator">
<footer class="footer">
  <div class="footer__general">
    <a href="#" class="to-top"><span class="hidden">naar boven</span></a>
    <h3 class="hidden">contact.</h3>
    <p class="footer__item footer__item_wide">Splitsing Koopvaardijlaan – Afrikalaan<br>
      9000 Gent<br>
      <a href="mailto:info@dokgent.be" class="link_disabled">info@dokgent.be</a>
      09 224 19 40
    </p>
    <p class="footer__item footer__item_wide">DOKvzw (postadres)<br>
      Toekomststraat 7<br>
      9040 Sint-Amandsberg
    </p>
  </div>
  <div class="footer__contact">
    <h3 class="hidden">sponsors.</h3>
    <?php
      $footerData = [
        [
          'title' => 'Co&ouml;rdinatie',
          'name' => 'Liesbeth Vlerick',
          'mail' => 'liesbeth@dokgent.be'
        ],
        [
          'title' => 'Techniek, Horeca',
          'name' => 'Tomas Lootens',
          'mail' => 'tomas@dokgent.be'
        ],
        [
          'title' => 'Vrijwilligerswerking',
          'name' => 'Celine De Coninck',
          'mail' => 'celine@dokgent.be'
        ],
        [
          'title' => 'Logistiek',
          'name' => 'Yoshi Dooms',
          'mail' => 'yoshi@dokgent.be'
        ],
        [
          'title' => 'Communicatie',
          'name' => 'Bert Bossaert',
          'mail' => 'bert@dokgent.be'
        ]
      ];

      foreach ($footerData as $item) {
        echo '<p class="footer__item">';
          echo '<strong>' . $item['title'] . '</strong>';
          echo '<span>' . $item['name'] . '</span>';
          echo '<a href="mailto:' . $item['mail'] . '">' . $item['mail'] . '</a>';
        echo '</p>';
      }
    ?>
  </div>
  <div class="sponsors">
    <?php
      $images = [
        ['name' => 'democrazy', 'url' => 'http://democrazy.be'],
        ['name' => 'cirq', 'url' => 'https://www.cirq.be'],
        ['name' => 'bionade', 'url' => 'http://bionade.de'],
        ['name' => 'eulala', 'url' => 'http://dokgent.be/1'],
        ['name' => 'gent', 'url' => 'https://stad.gent'],
        ['name' => 'pepsi', 'url' => 'http://www.pepsi.com/nl-be'],
        ['name' => 'sogent', 'url' => 'http://sogent.be'],
        ['name' => 'thuis', 'url' => 'http://www.thuisindestad.be'],
        ['name' => 'vedett', 'url' => 'http://vedett.be'],
        ['name' => 'overheid', 'url' => 'http://www.vlaanderen.be/nl'],
        ['name' => 'biofresh', 'url' => 'http://biofresh.be']
      ];
      foreach ($images as $image) {
        echo '<a href="' . $image['url'] . '" target="_blank" class="sponsors__item sponsors__item_' . $image['name'] . '">';
          echo '<span class="hidden">sponsor ' . $image['name'] . ' link</span>';
        echo '</a>';
      }
    ?>
  </div>
</footer>
