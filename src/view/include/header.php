<header class="header">
  <h1 class="header__title"><span class="hidden">DOK Gent</span></h1>
  <div>
    <nav class="header__nav">
      <h2 class="hidden">navigatie</h2>
      <a class="header__nav__item" href="<?php echo ROOT ?>">HOME</a>
      <a class="header__nav__item" href="<?php echo ROOT ?>events">EVENTS</a>
      <a class="header__nav__item link_disabled" href="<?php echo ROOT ?>blog">BLOG</a>
      <a class="header__nav__item link_disabled" href="<?php echo ROOT ?>info">INFO</a>
    </nav>
    <div class="select">
      <select class="select__dropdown" name="language-select">
        <option disabled >taal</option>
        <option disabled>──────────</option>
        <option value="dutch">nederlands</option>
        <option value="english">english</option>
      </select>
    </div>
  </div>
</header>
