<?php
  require_once WWW_ROOT . 'view' . DS . 'include' . DS . 'header.php';
  define('SRC', 'events/' . $event['id']);
?>
<main class="content eventdetail">


  <section class="event-detail">
    <div class="event-page__top">
      <h2 class="title title_type_big title_color_white">event.</h2>
			<a href="<?php echo ROOT ?>events" class="button">terug naar overzicht</a>
    </div>
    <article class="event-page__detail">
      <header class="event-page__detail__header">
        <h3 class="title title_type_medium"><?php echo $event['title'] ?></h3>
        <div class="event-page__detail__tags">
          <div class="event-page__detail__date">
            <div class="event-page__detail__left">
              <span class="event-page__detail__span">van</span>
              <span class="tag"><?php echo date('d-m / H:i', strtotime($event['start'])) ?></span>
            </div>
            <div class="event-page__detail__left">
              <span class="event-page__detail__span">tot</span>
              <span class="tag"><?php echo date('d-m / H:i', strtotime($event['end'])) ?></span>
            </div>
          </div>
          <div class="event-page__detial__extra">
            <div class="event-page__detail__right">
              <span class="event-page__detail__span">locatie</span>
              <div>
                <?php
                  foreach ($event['locations'] as $location) {
                    echo '<span class="tag tag_color_' . $location['color'] . '">' . $location['name'] . '</span>';
                  }
                ?>
              </div>
            </div>
            <div class="event-page__detail__right">
              <span class="event-page__detail__span">organisator</span>
              <span class="tag"><?php echo $event['organiser'] ?></span>
            </div>
          </div>
        </div>
      </header>
      <section class="event-page__detail__content">
				<h3 class="hidden">event details</h3>
        <div class="event-page__detail__content__text">
          <?php echo $event['description'] ?>
        </div>
        <div class="event-page__detail__content__images">
          <?php
            foreach ($event['images'] as $image) {
              echo '<img class="event-page__detail__content__image" src="' . ROOT . 'assets/img/events/' . $image . '.jpg" alt="' . $event['title'] . '">';
            }
          ?>
        </div>
      </section>
    </article>
  </section>


  <?php require_once WWW_ROOT . 'view' . DS . 'include' . DS . 'footer.php' ?>
