<?php
	require_once WWW_ROOT . 'view' . DS . 'include' . DS . 'header.php';
	define('SRC', 'events');

	$filter = 'alle events';
	if (isset($events['filter'])) {
		$filter = $events['filter'];
	}
?>
<main class="content eventlist">
  <section class="events-page">
    <div class="event-page__top">
      <h2 class="title title_type_big title_color_white">events.</h2>
    </div>
    <div class="event-page__filter">
      <div class="event-page__filter__top">
				<div class="filter filter_type_month">
					<form class="filter__form" action="<?php echo ROOT ?>events" method="post">
						<div class="select filter__form__option">
				      <select class="select__dropdown" name="month">
								<option value="-1">selecteer maand</option>
								<option disabled>──────────</option>
								<option value="05">mei</option>
				        <option value="06">juni</option>
								<option value="07">juli</option>
								<option value="08">augustus</option>
								<option value="09">september</option>
				      </select>
				    </div>
						<button class="submit" type="submit" name="submit" value="submit">FILTER</button>
					</form>
	      </div>
				<div class="filter filter_type_name">
					<form class="filter__form" action="<?php echo ROOT ?>events" method="post">
						<input class="input filter__form__option" type="text" name="name" placeholder="event naam">
						<button class="submit" type="submit" name="submit" value="submit">FILTER</button>
					</form>
	      </div>
      </div>
			<?php
				if (isset($events['filter'])) {
					echo '<div class="event-page__filter__bottom filter-active">';
					echo '<span class="filter__status filter__status title">';
						echo $filter;
						echo '<a href="' . ROOT . 'events" class="filter__status__close">&times;</a>';
					echo '</span>';
				} else {
					echo '<div class="event-page__filter__bottom">';
					echo '<span class="filter__status filter__status title">';
					 	echo $filter;
					echo '</span>';
				}
				echo '</div>';
			?>
    </div>
    <?php
			$data = '';
			if (isset($events['eventCount'])) {
				$data = 'data-eventcount="' . $events['eventCount'] . '"';
			}
			echo '<div class="event-page__list" ' . $data . '>';
      foreach ($events['events'] as $event) {
        echo '<a href="' . ROOT . 'events/' . $event['id'] . '">';
          echo '<article class="event-article">';
            if ($event['image'] !== null) {
							echo '<picture class="event-article__image">';
							  echo '<source type="image/webp" srcset="' . ROOT . 'assets/img/events/' . $event['image'] . '.webp">';
							  echo '<img src="' . ROOT . 'assets/img/events/' . $event['image'] . '.jpg" class="event-article__image" alt="' . $event['title'] . '">';
							echo '</picture>';
            } else {
              echo '<div class="event-article__placeholder"></div>';
            }
            echo '<div class="event-article__content">';
              echo '<h3 class="title event-article__title">' . $event['title'] . '</h3>';
              echo '<div class="tags">';
                foreach ($event['locations'] as $location) {
                  echo '<span class="tag tag_color_' . $location['color'] . '">' . $location['name'] . '</span>';
                }
                echo '<span class="tag">' . date('d-m', strtotime($event['start'])) . ' / ' . date('d-m', strtotime($event['end'])) . '</span>';
              echo '</div>';
              echo '<p class="event-article__paragraph">' . $event['preview'] . '</p>';
            echo '</div>';
          echo '</article>';
        echo '</a>';
      }
      echo '</div>';
	    if (isset($events['eventCount'])) {
				$classes = 'button button_color_orange ajax-load-more';
				if (isset($_GET['items']) && $_GET['items'] === 'all') {
					$classes = 'hidden button button_color_orange ajax-load-more';
				}
				echo '<a href="' . ROOT . 'events/all" class="' . $classes . '">laad meer items</a>';
			}
		?>
  </section>
  <?php require_once WWW_ROOT . 'view' . DS . 'include' . DS . 'footer.php' ?>
