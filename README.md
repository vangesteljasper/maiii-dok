[[ School Assignment ]]

# MAJOR_DOK

this is my private repository containing my major assignment regarding DOK GENT

## website

[http://student.howest.be/jasper.van.gestel/20162017/ma3/dok](http://student.howest.be/jasper.van.gestel/20162017/ma3/dok)

## development

`$ yarn run dev`

## production

for localhost

`$ yarn run build`

for http://student.howest.be/jasper.van.gestel/20162017/ma3/dok

`$ yarn run build_`
