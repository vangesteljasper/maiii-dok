const
  gulp = require(`gulp`),
  replace = require(`gulp-replace`),
  rename = require(`gulp-rename`),
  path = process.env.DEST_PATH || `/`,
  chalk = require(`chalk`);

console.log(chalk.yellow(`ROOT: \'${path}\'`));

gulp.task(`change_path_php`, () => {
  gulp.src(`./dist/index.php`)
    .pipe(replace(`define('ROOT', '/');`, `define('ROOT', '${path}');`))
    .pipe(rename(`./dist/index.php`))
    .pipe(gulp.dest(`./`));
});

gulp.task(`change_path_js`, () => {
  gulp.src(`./src/js/main/script.js`)
    .pipe(replace(/const ROOT =.*;/g, `const ROOT = \`${path}\`;`))
    .pipe(rename(`./src/js/main/script.js`))
    .pipe(gulp.dest(`./`));
});

gulp.task(`revert_path_js`, () => {
  gulp.src(`./src/js/main/script.js`)
    .pipe(replace(/const ROOT =.*;/g, `const ROOT = \`/\`;`))
    .pipe(rename(`./src/js/main/script.js`))
    .pipe(gulp.dest(`./`));
});
